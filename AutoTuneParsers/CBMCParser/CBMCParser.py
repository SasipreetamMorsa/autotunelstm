import re, argparse

parser = argparse.ArgumentParser(description='Combine Configurations and Program code')
parser.add_argument('statsAll', metavar='statsAll.txt', type=str, help='path to the statsAll.txt file, which contains the configurations and program names')
args = parser.parse_args()
statsAll = args.statsAll
statsfile = open(statsAll, 'r')

list = []
for line in statsfile:
    line = line.rstrip("\n\r")
    line = re.split(',| ', line)
    line.pop(-2)
    if line[-1] == '10' or line[-1] == '0':
        line[-1] = 'truepositive'
    else:
        line[-1] = 'falsepositive'
    list.append(line)

# Create dataFile
# Comment out if datafile already created
dataFile = open('dataFile.txt', 'w')
while len(list) != 0:
    print("opening file "+list[0][-2])
    with open('c/'+list[0][-2], 'r') as programFile:
        program = programFile.read().replace('\n', ' ')
        list[0][-2] = program
        for i in range(len(list[0])):
            dataFile.write(list[0][i])
            dataFile.write(' ')
        dataFile.write('\n')
        list.pop(0)

dataFile.close()

vocab = {}

f = open('dataFile.txt', 'r')
counter = 1
data = []
while counter <= 295000:
    for lines in range(100):
        data.append(f.readline().split())
        counter += 1
    for i in range(len(data)):
        for token in data[i]:
            key = token.strip()
            if vocab.has_key(key):
                val = vocab[key]
                vocab[key] = (val+1)
            else:
                vocab[key] = 1
    data = []

wordID = 3
with open('cbmcDict.txt', 'w') as vocabFile:
    for key, value in vocab.iteritems():
        vocabFile.write(key +' '+ str(wordID))
        vocabFile.write('\n')
        wordID = wordID + 1