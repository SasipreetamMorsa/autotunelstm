import argparse

parser = argparse.ArgumentParser(description='Split Data File into multiple files')
parser.add_argument('dataFile', metavar='dataFile.txt', type=str, help='path to dataFile.txt')
args = parser.parse_args()
dataFile = args.dataFile

lines_per_file = 29500
smallfile = None
i = 1
with open(dataFile) as bigfile:
    for lineno, line in enumerate(bigfile):
        if lineno % lines_per_file == 0:
            if smallfile:
                smallfile.close()
            small_filename = 'small_file_{}.txt'.format(i)
            i += 1
            smallfile = open(small_filename, "w")
        smallfile.write(line)
    if smallfile:
        smallfile.close()